import '../assets/style.css'
import { monaco } from './editor';

const queryService = require('./service/queryService')
const solutionResultModule = require('./modules/solutionResultModule')
const consoleModule = require('./modules/consoleModule')
const graphModule = require('./modules/graphModule')

const theoryField = document.querySelector("#theory");
const queryField = document.querySelector("#query");
const solutionsList = document.querySelector("#solutions");

const outputConsole = document.querySelector("#console");
const graphContainer = document.querySelector("#graph");


let theoryEditor = monaco.editor.create(theoryField, {
    value: [
        'graphBuildMode(base).',
        'statementLabellingMode(base).',
        'argumentLabellingMode(grounded).',
        'orderingPrinciple(last).',
        'orderingComparator(normal).',
        '',
        'd1 : bird(X) => flies(X).',
        'd2 : penguin(X) => bird(X).',
        's1 : penguin(X) -> -flies(X).',
        'a1 :-> penguin(tweety).'
    ].join('\n'),
    language: 'tuprolog',
    minimap: {
        enabled: false
    }
});


let queryEditor = monaco.editor.create(queryField, {
    value: 'buildLabelSets',
    language: 'tuprolog',
    minimap: {
        enabled: false
    }
});


function startup() {
    setListeners(_ => {
        const resultManager = solutionResultModule.init(solutionsList);
        const consoleManager = consoleModule.init(outputConsole);
        const graphManager = graphModule.init(graphContainer);

        consoleManager.clear()
        graphManager.clear()

        const { i, query, solver } = queryService
            .solve(theoryEditor.getValue(), queryEditor.getValue(), consoleManager.outputConsumer());

        resultManager.printSolution(i, query, () => {
            document.querySelector(".overlay").style.display = "block"
        }, () => {
            graphManager.printGraph(queryEditor.getValue(), solver);
            document.querySelector(".overlay").style.display = "none"
        });
    });
}

function setListeners(solveAction) {
    document.querySelector("button.solve")
        .addEventListener("click", () => {
            try {
                solveAction()
            }
            catch (error) {
                alert(`${error.name.toUpperCase()} \n${error.message}`)
            }
        });

    document
        .querySelector("#inputFile")
        .addEventListener("change", e =>
            readFile(e.target.files[0], text => theoryEditor.setValue(text))
        );
}

function readFile(file, cb) {
    var reader = new FileReader();
    reader.onload = (function (reader) {
        return () => cb(reader.result);
    })(reader);
    reader.readAsText(file);
};

startup();


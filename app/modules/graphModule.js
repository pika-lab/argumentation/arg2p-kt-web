function GraphModule() {

    function init(container) {
        prepareContainer(container)
        return { printGraph, clear }
    }

    function printGraph(query, solver) {
        if (!checkQuery(query)) return
        print(solver)
    }

    function clear() {
        document.querySelector("#graph-theory").innerHTML = ""
        document.querySelector("#graph-visual").innerHTML = ""
    }

    function prepareContainer(container) {
        container.innerHTML = `
            <div class="graph-container">
                <div id="graph-theory" class="graph-text"></div>
                <div id="graph-visual" class="graph-drawing"></div>
            </div>
        `
    }

    function checkQuery(query) {
        return query.startsWith("buildLabelSets")
    }

    function print(solver) {

        function _printTheory(argumentList) {

            function _format(descriptor) {
                return '<div style="font-weight:bold; overflow:hidden;">' + 
                    descriptor + '</div>'
            }

            document.querySelector("#graph-theory").innerHTML = argumentList
                .reduce((a, b) => a + _format(b.descriptor), "")
        }

        function _printGraph(argumentList, attacks) {
            
            function _toInt(identifier) {
                return parseInt(identifier.substring(1))
            }

            function _toColor(labelling) {
                switch(labelling) {
                    case "in" : return "#00ff00"
                    case "out" : return "#ff0000"
                    case "und" : return "#808080"
                }
            }

            const vis = require("vis-network/standalone")

            const network = new vis.Network(document.querySelector("#graph-visual"), {
                nodes: new vis.DataSet(argumentList.map(x => {
                    return {
                        id: _toInt(x.identifier),
                        label: x.identifier,
                        color: _toColor(x.label)
                    }
                })),
                edges: new vis.DataSet(attacks.map(x => {
                    return {
                        from: _toInt(x.attacker),
                        to: _toInt(x.attacked),
                        arrows: "to"
                    }
                }))
            }, {})

            network.fit()
        }

        let { mining } = require('../common').tuprolog;

        const argumentSequence = mining.argument.Companion.mineArguments(solver)
        const attackList = getSolutions(mining.attack.Companion.mineAttacks(solver, argumentSequence).iterator())
        const argumentList = getSolutions(argumentSequence.iterator())
        
        _printTheory(argumentList)
        _printGraph(argumentList, attackList)
    }

    return { init }
}

module.exports = GraphModule()

function getSolutions(iterator) {
    const sol = []
    while(iterator.hasNext()) {
        sol.push(iterator.next())
    }
    return sol
}
function queryService() {

    const common = require('../common.js')
    const ClassicSolverFactory = common.tuprolog.classic.ClassicSolverFactory;
    const outputChannel = common.tuprolog.solve.channel.OutputChannel;

    function argOperators(theoryText) {
        return `
            :- op(1199, xfx, ':->').
            :- op(1199, xfx, ':=>').
            :- op(1199, xfx, '=>').
            :- op(1001, xfx, ':').
        ` + theoryText
    }
    
    function solverOf(staticKb, outputConsumer) {
        return ClassicSolverFactory.mutableSolverWithDefaultBuiltinsAnd(
            ClassicSolverFactory.defaultLibraries.plus(common.tuprolog.solve.library.Libraries.Companion.of([common.tuprolog.arg2p])),
            ClassicSolverFactory.defaultFlags,
            staticKb,
            ClassicSolverFactory.defaultDynamicKb,
            ClassicSolverFactory.defaultInputChannel,
            outputChannel.Companion.of(outputConsumer),
            outputChannel.Companion.of(_ => { }),
            outputChannel.Companion.of(_ => { }))
    }

    function solve(theoryText, queryText, outputConsumer) {

        if (/^\s*$/.test(queryText)) {
            throw {
                name: "Query error",
                message: "Query is a mandatory field."
            }
        }

        const query = tryBlock(() => common.tuprolog.core.parsing.parseStringAsStruct(queryText), "Query Error");
        const theory = tryBlock(() => common.tuprolog.theory.parsing.parseAsTheory(argOperators(theoryText)), "Theory Error");
        const solver = solverOf(theory, outputConsumer);
        const solutions = tryBlock(() => solver.solve(query), "Solve Error");
        const i = solutions.iterator();
        return { i, query, solver };
    }

    function tryBlock(fun, name) {
        try {
            return fun();
        } catch (err) {
            throw {
                name,
                message: formatErrorMessage(err)
            }
        }
    }

    function formatErrorMessage(err) {
        let message = '';
        message += err.name ? `${err.name}\n` : '';
        message += err.message ? `${err.message}\n` : '';
        message += err.line ? `at line ${err.line}` : '';
        return message
    }


    return { solve }
}

module.exports = queryService()
